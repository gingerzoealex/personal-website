import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import VuePaginate from 'vue-paginate'

// Page content
import Experience from '@/components/Experience'
import Home from '@/components/Home'
import About from '@/components/About'
import Skills from '@/components/Skills'
import Contact from '@/components/Contact'
// Fallback page
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)
Vue.use(VueResource)
Vue.use(VuePaginate)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Experience',
      name: 'Experience',
      component: Experience
    },
    {
      path: '/About',
      name: 'About',
      component: About
    },
    {
      path: '/Skills',
      name: 'Skills',
      component: Skills
    },
    {
      path: '/Contact',
      name: 'Contact',
      component: Contact
    }
  ]
})
