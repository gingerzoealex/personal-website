Clone the repo

```git clone https://gitlab.com/gingerzoealex/personal-website.git```

Run the docker container

```docker run -it -p 8080:8080 --rm --name dockerize-vuejs-app-1 vuejs-cookbook/dockerize-vuejs-app```
